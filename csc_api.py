from typing import List
import warnings


def get_lingering_users() -> List[str]:
    warnings.warn(f"{__name__} not implemented")


def create_vhost(domain: str, port: int, ip: str):
    warnings.warn(f"{__name__} not implemented")


def get_ip() -> str:
    warnings.warn(f"{__name__} not implemented")

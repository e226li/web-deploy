import glob
import subprocess
import csc_api


def main():
    users = csc_api.get_lingering_users()

    for user in glob.glob('/var/lib/systemd/linger/*'):
        if user not in users:
            subprocess.run(['loginctl', 'disable-linger', user])

    for user in users:
        if user not in glob.glob('/var/lib/systemd/linger/*'):
            subprocess.run(['loginctl', 'enable-linger', user])


if __name__ == '__main__':
    main()

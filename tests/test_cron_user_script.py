import sys
import os
import unittest
from unittest.mock import patch, call

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/..")

import cron_script


@patch('csc_api.get_lingering_users')
@patch('subprocess.run')
@patch('glob.glob')
class TestCronScript(unittest.TestCase):
    def setUp(self):
        self.users = ['one', 'two', 'three', 'four']

    def test_create_new_lingering(self, glob_mock, shell_mock, user_mock):
        user_mock.return_value = self.users
        glob_mock.return_value = []

        cron_script.main()

        assert shell_mock.call_args_list == [call(['loginctl', 'enable-linger', x]) for x in user_mock.return_value]

    def test_delete_lingering(self, glob_mock, shell_mock, user_mock):
        user_mock.return_value = []
        glob_mock.return_value = self.users

        cron_script.main()

        assert shell_mock.call_args_list == [call(['loginctl', 'disable-linger', x])
                                             for x in glob_mock.return_value]

    def test_create_and_delete_lingering(self, glob_mock, shell_mock, user_mock):
        user_mock.return_value = self.users[1::2]
        glob_mock.return_value = self.users[::2]

        cron_script.main()

        print(shell_mock.call_args_list)
        self.assertCountEqual(shell_mock.call_args_list, [call(['loginctl', 'enable-linger', x])
                                                          for x in self.users[1::2]] +
                              [call(['loginctl', 'disable-linger', x]) for x in self.users[::2]])

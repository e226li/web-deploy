# Easier web deployments

# Usage

Consumes an API (`import csc_api`) with the following functions:

- `get_lingering_users() -> List[str]`
- `create_vhost(domain: str, port: int, ip: str) -> bool`
- `get_ip() -> str`

Dummy `csc_api` lives in `./csc_api.py`

Test with
```bash
pytest
```
import argparse
import logging
import os
import sys
import subprocess
from typing import List

import csc_api
import jinja2


def parse_args(arg_parser: argparse.ArgumentParser, arg_list: list) -> argparse.Namespace:
    return arg_parser.parse_args(arg_list)


def install(name, command, domain, port, ip, directory, env) -> subprocess.CompletedProcess:
    template_loader = jinja2.FileSystemLoader(searchpath="./")
    template_env = jinja2.Environment(loader=template_loader, autoescape=True)
    template = template_env.get_template('systemd_template.j2')
    output_text = template.render(command=command, port=port, ip=ip, directory=directory, env=env)

    home_dir = os.path.expanduser('~')
    sanitized_path = os.path.join(home_dir, '.config/systemd/user/', os.path.basename(name) + '.service')
    logging.info(f"Writing unit file to {sanitized_path}")
    if os.path.isfile(sanitized_path):
        raise FileExistsError(f"Try running `rm -f {sanitized_path}`")
    if not os.path.exists(os.path.dirname(sanitized_path)):
        logging.info(f"Recursively making directory {os.path.dirname(sanitized_path)}")
        os.makedirs(os.path.dirname(sanitized_path))
    with open(sanitized_path, 'w') as f:
        f.write(output_text)
    return_process = subprocess.run(['systemctl', '--user', 'enable', name, '--now'])

    csc_api.create_vhost(domain, port, ip)

    return return_process


def uninstall(name: str) -> List[subprocess.CompletedProcess]:
    return_processes = list()
    return_processes.append(utility(name, 'stop'))
    return_processes.append(utility(name, 'disable'))
    home_dir = os.path.expanduser('~')
    sanitized_path = os.path.join(home_dir, '.config/systemd/user/', os.path.basename(name) + '.service')
    logging.info(f"Deleting {sanitized_path}")
    os.remove(sanitized_path)
    return return_processes


def utility(name: str, action: str, reload: bool = True) -> subprocess.CompletedProcess:
    if reload is True: # may lead to unnecessary reloads, but impact should be minimal because of --user
        subprocess.run(['systemctl', '--user', 'daemon-reload'])
    return subprocess.run(['systemctl', '--user', action, name])


parser = argparse.ArgumentParser(prog='Deployment Helper')
parser.add_argument('-v', '--verbose', action='store_true')
subparsers = parser.add_subparsers(help='command help', dest='command')

parser_install = subparsers.add_parser('install', help='a help')
parser_install.add_argument('name')
parser_install.add_argument('to_run')
parser_install.add_argument('domain')
parser_install.add_argument('port')
parser_install.add_argument('ip', nargs='?', default=csc_api.get_ip())
parser_install.add_argument('--directory', nargs='?', default=os.getcwd())
parser_install.add_argument('--env', nargs='*')

parser_uninstall = subparsers.add_parser('uninstall', help='a help')
parser_uninstall.add_argument('name')

parser_start = subparsers.add_parser('start', help='a help')
parser_start.add_argument('name')

parser_stop = subparsers.add_parser('stop', help='a help')
parser_stop.add_argument('name')

parser_restart = subparsers.add_parser('restart', help='a help')
parser_restart.add_argument('name')

parser_reload = subparsers.add_parser('reload', help='a help')
parser_reload.add_argument('name')

parser_enable = subparsers.add_parser('enable', help='a help')
parser_enable.add_argument('name')

parser_disable = subparsers.add_parser('disable', help='a help')
parser_disable.add_argument('name')

args = parse_args(parser, sys.argv[1:])

if args.verbose:
    from pip._internal.operations import freeze

    logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
    logging.debug(list(freeze.freeze()))
    logging.debug(args)
else:
    logging.basicConfig(format="%(levelname)s: %(message)s")

if args.command == 'install':  # can use switch case, but I think ceo might be using 3.9
    args_list = [args.name, args.to_run, args.domain, args.port, args.ip, args.directory, args.env]
    logging.info(f"Running install(*{args_list})")
    subprocess_return = install(*args_list)
    logging.info(subprocess_return.stdout if subprocess_return.stdout is not None else subprocess_return.returncode)
elif args.command == 'uninstall':
    logging.info(f"Running uninstall(*['{args.name}'])")
    subprocess_returns = uninstall(args.name)
    for subprocess_return in subprocess_returns:
        logging.info(subprocess_return.stdout if subprocess_return.stdout is not None else subprocess_return.returncode)
elif args.command in ['start', 'stop', 'restart', 'reload', 'enable', 'disable']:
    logging.info(f"Running utility(*{[args.name, args.command]}'")
    subprocess_return = utility(args.name, args.command)
    logging.info(subprocess_return.stdout if subprocess_return.stdout is not None else subprocess_return.returncode)
